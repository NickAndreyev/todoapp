import React, {useState} from 'react';
import MyInput from "./UI/input/MyInput";
import MyButton from "./UI/button/MyButton";

const PostForm = ({create, postNumber}) => {

    const [post, setPost] = useState({title: "", body: ""})

    const addNewPost = (event) => {
        event.preventDefault()
        const newPost = {...post, date: new Date().getHours() + ":" + new Date().getMinutes(), id : postNumber + 1}
        create(newPost)
        setPost({title: "", body: ""})
    }

    return (
        <form>
            <MyInput
                value = {post.title}
                onChange = {event => setPost({...post, title: event.target.value})}
                type = "text"
                placeholder="Title"
            />
            <MyInput
                value = {post.body}
                onChange = {event => setPost({...post, body: event.target.value})}
                type = "text"
                placeholder="Content"
            />
            <MyButton onClick = {addNewPost}>Add</MyButton>
        </form>
    );
};

export default PostForm;