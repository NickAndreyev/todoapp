import React, {useState} from 'react';
import classes from "./UI/checkbox/MyCheckbox.module.css"
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import cl from "./UI/label/MyLabel.module.css"
import MyButton from "./UI/button/MyButton";

const PostItem = (props) => {

    const[checkBoxState, setCheckBoxState] = useState(true);

    return (
        <div className="post">
            <label className={classes.myCheckbox}>
                <input
                    onClick={() => {checkBoxState ? setCheckBoxState(false) : setCheckBoxState(true)}}
                    type="checkbox"
                    value="value-1"
                /><span></span>
            </label>
            <div className="post__content" style={checkBoxState ? {textDecoration: "none"} : {textDecoration: "line-through"}}>
                <strong> {props.post.id}. {props.post.title}</strong>
                <hr className="solid"/>
                <div>
                    {props.post.body}
                </div>

            </div>
            <div className={cl.myLabel}>{props.post.date}</div>
            <div className="post__buttons">
                <MyButton onClick = {() => props.remove(props.post)} icon = {DeleteForeverOutlinedIcon}/>
            </div>
        </div>
    );
};

export default PostItem;