import React from 'react';
import MyInput from "./UI/input/MyInput";
import MySelect from "./UI/select/MySelect";

const PostFilter = ({filter, setFilter}) => {
    return (
        <div>
            <MyInput
                type = "search"
                value={filter.searchString}
                onChange={event => setFilter({...filter, searchString: event.target.value})}
                placeholder="Search..."
            />
            <MySelect
                value={filter.sortType}
                onChange={selectedSortType => setFilter({...filter, sortType: selectedSortType})}
                defaultValue="Filter"
                options={[
                    {value: 'title', name: 'By title'},
                    {value: 'body', name: 'By content'},
                    {value: 'date', name: 'By create date'}
                ]}
            />
        </div>
    );
};

export default PostFilter;