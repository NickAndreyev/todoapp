import React from 'react';
import classes from './MyModal.module.css'

const MyModal = ({children, visibility, setVisibility}) => {

    const rootClasses = [classes.myModal]
    if (visibility) {
        rootClasses.push(classes.active)
    }

    return (
        <div onClick={() => setVisibility(false)} className={rootClasses.join(' ')}>
            <div onClick={(event) => event.stopPropagation()} className={classes.myModalContent}>
                {children}
            </div>
        </div>
    );
};

export default MyModal;