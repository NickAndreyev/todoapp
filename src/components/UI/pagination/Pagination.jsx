import React from 'react';
import {getPagesArray} from "../../../utils/pages";

const Pagination = ({totalPages, currentPage, changePage}) => {

    let pagesArray = getPagesArray(totalPages);

    return (
        <div className="page__wrapper">
            {pagesArray.map(page =>
                <span
                    onClick={() => changePage(page)}
                    key = {page}
                    className={currentPage === page ? 'page page__current' : 'page'}
                >
                    {page}
                </span>
            )}
        </div>
    );
};

export default Pagination;