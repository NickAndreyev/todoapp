import React from 'react';
import {Link} from "react-router-dom";
import MyButton from "../button/MyButton";

const MyNavbar = () => {

    return (
        <div className="navbar">
            <div className="navbar__links">
                <Link to="/todos">
                    <MyButton
                        style={{marginRight : 8}}
                        type="button"
                    >TODOs</MyButton>
                </Link>
                <Link to="/about">
                    <MyButton style={{marginRight : 20}}>About</MyButton>
                </Link>
            </div>
        </div>
    );
};
export default MyNavbar;