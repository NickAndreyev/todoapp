import React from 'react';
import classes from './MyButton.module.css'
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';

const MyButton = ({children, icon, ...props}) => {
    return (
        <button
            {...props} className={classes.myButton}
        >
            {icon &&
                (icon === DeleteForeverOutlinedIcon
                    ? <DeleteForeverOutlinedIcon className={classes.myButton__icon}/>
                    : <AddCircleOutlineOutlinedIcon className={classes.myButton__icon}/>)
            }

            {children}
        </button>
    );
};

export default MyButton;