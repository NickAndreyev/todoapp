import {useMemo} from "react";

export const useSortedPosts = (posts, sortType) => {

    return useMemo(() => {
        if (sortType) {
            return [...posts].sort((a, b) => a[sortType].localeCompare(b[sortType]))
        } else
            return posts;

    }, [sortType, posts]);
}

export const usePosts = (posts, sortType, searchString) => {

    const sortedPosts = useSortedPosts(posts, sortType);

    return useMemo(() => {
        return sortedPosts.filter(post => post.title.toLowerCase().includes(searchString.toLowerCase()) || post.body.toLowerCase().includes(searchString.toLowerCase()))
    }, [searchString, sortedPosts]);
}