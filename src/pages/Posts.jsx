import React, {useEffect, useState} from 'react';
import {usePosts} from "../hooks/usePosts";
import MyButton from "../components/UI/button/MyButton";
import MyModal from "../components/UI/modal/MyModal";
import PostForm from "../components/PostForm";
import PostFilter from "../components/PostFilter";
import Pagination from "../components/UI/pagination/Pagination";
import MyLoader from "../components/UI/loader/MyLoader";
import PostList from "../components/PostList";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";


function Posts() {
    const [posts, setPosts] = useState([]);

    const [filter, setFilter] = useState({sortType: "", searchString: ""});
    const [modal, setModal] = useState(false);
    const [pageNumber, setPageNumber] = useState(1);

    const [isLoading, setIsLoading] = useState(false);

    const  getPosts = async () => {
        setIsLoading(true);
        await new Promise(r => setTimeout(r, 800));
        setPosts([{
                id: 1,
                date: "11:54",
                title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            },
            {
                id: 2,
                date: "09:21",
                title: "qui est esse",
                body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
            },
            {
                id: 3,
                date: "07:48",
                title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
                body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
            },
            {
                id: 4,
                date: "21:16",
                title: "eum et est occaecati",
                body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
            }])
        setIsLoading(false);
    }

    const sortedSearchedPosts = usePosts(posts, filter.sortType, filter.searchString);

    useEffect(() => {
        getPosts()
    }, [pageNumber])


    const createPost = (newPost => {
        setPosts([...posts, newPost]);
        setModal(false);
    })

    const removePost = (postToRemove) => {
        setPosts(posts.filter(p => p.id !== postToRemove.id));
    }

    const changePage = (page) => {
        setPageNumber(page);
    }

    return (
        <div className="App">
            <MyModal
                visibility={modal}
                setVisibility={setModal}
            >
                <PostForm
                    create={createPost}
                    postNumber={posts.length}
                />
            </MyModal>

            <div className="buttons-container">
                <PostFilter
                    filter={filter}
                    setFilter={setFilter}
                />
                <MyButton
                    icon = {AddCircleOutlineOutlinedIcon}
                    onClick={() => setModal(true)}>
                </MyButton>
            </div>

            {isLoading
                ? <MyLoader/>
                : <PostList
                    posts={sortedSearchedPosts}
                    title = "TODO List"
                    remove={removePost}
                />
            }

            <div className="root">
            <Pagination
                totalPages={3}
                currentPage={pageNumber}
                changePage={changePage}
            />
            </div>
        </div>
    );
}

export default Posts;